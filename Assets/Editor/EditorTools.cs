﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EditorTools : Editor
{
   [MenuItem("Tools/更改位置")]
    public static void AddCollider()
    {
        GameObject[] objs= Selection.gameObjects;
        foreach (var go in objs)
        {
            //go.transform.localPosition = new Vector3(go.transform.localPosition.x,0, go.transform.localPosition.z);
            //go.transform.localScale = new Vector3(go.transform.localScale.x,10,1);
            //go.AddComponent<BoxCollider>();
            go.transform.localEulerAngles = new Vector3(0, go.transform.localEulerAngles.y, 0);
        }
    }
    [MenuItem("Tools/隐藏Mesh")]
    public static void HideMesh()
    {
        GameObject[] objs = Selection.gameObjects;
        foreach (var go in objs)
        {
            Renderer render = go.GetComponent<Renderer>();
            if (render)
            {
                render.enabled = false;
            }
        }
    }
}
