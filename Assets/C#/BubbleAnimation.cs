﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleAnimation : MonoBehaviour
{
    private float runTime;

    private RectTransform rectTransform;

    private Vector2 startRect;
    private Vector2 endRect;
    // Start is called before the first frame update
    void Start()
    {
        rectTransform = this.gameObject.GetComponent<RectTransform>();

        startRect = rectTransform.sizeDelta;
        endRect = Vector2.one * 120;

        totalTime = Random.Range(5, 20);
    }
    Vector2 sizeDelta;
    private int direction = 1;
    private float totalTime = 8;
    // Update is called once per frame
    void Update()
    {
        runTime += Time.deltaTime;

        if (runTime > totalTime)
        {
            runTime = 0;
            // direction *= -1;

            sizeDelta = startRect;
            startRect = endRect;
            endRect = sizeDelta;
        }

        sizeDelta = Vector2.Lerp(startRect, endRect, runTime / totalTime);

        rectTransform.sizeDelta = sizeDelta;
    }
}
