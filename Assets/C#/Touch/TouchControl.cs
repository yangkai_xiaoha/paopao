﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControl : MonoBehaviour
{
    //半径
    private float radio;
    public Transform target;
    private BubbleControl bubbleControl;
    // Start is called before the first frame update
    void Start()
    {
        radio = Vector3.Distance(this.transform.position,target.position);
        bubbleControl = this.gameObject.GetComponent<BubbleControl>();
    }
    //是否在点击状态
    private bool isClick = true;
    private float runTime;
    private bool Timing;
    //判断触摸的点是否在小球范围内
    public bool Is_Click(Vector3 world)
    {
        if(Vector3.Distance(world,this.transform.position) < radio)
        {
            //是点击了
            if(isClick)
            {
                //GameObject item = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                //item.transform.position = world;
                bubbleControl.OnClick();
                Timing = true;
                isClick = false;
                return true;
            }
        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Timing)
        {
            runTime += Time.deltaTime;
            if(runTime > 3)
            {
                Timing = false;
                runTime = 0;
                isClick = true;
            }
        }
        
    }
}
