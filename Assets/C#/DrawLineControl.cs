﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLineControl : MonoBehaviour
{
    public float radius = 5.6f;

    private List<Transform> list = new List<Transform>();

    private LineRenderer lineRenderer;

    public float difference = 1;

    public bool isForce = true;

    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = this.gameObject.GetComponent<LineRenderer>();
        GameObject item;
        GameObject root = new GameObject("Line"+this.gameObject.name); //0.21 -2.16 0
        root.transform.position = this.transform.position;
        for (int i = 0; i < 180; i++)
        {
            item = new GameObject(i.ToString());
            item.name = i.ToString();
            item.transform.localScale = Vector3.one * 0.1f;
            item.transform.position = root.transform.position + radius * Vector3.right * Mathf.Cos(i * Mathf.Deg2Rad * 2) + radius * Vector3.forward * Mathf.Sin(i * Mathf.Deg2Rad * 2);
            item.transform.forward = (root.transform.position - item.transform.position).normalized;
            list.Add(item.transform);
            item.AddComponent<ForceControl>().drawLineControl = this;
            //item.GetComponent<MeshRenderer>().enabled = false;
            item.transform.SetParent(root.transform);
        }
        OnDrawLine();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    //修正半径
        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        list[i].transform.position = this.transform.position + radius * Vector3.right * Mathf.Cos(i * Mathf.Deg2Rad * 4) + radius * Vector3.up * Mathf.Sin(i * Mathf.Deg2Rad * 4);
        //        list[i].transform.forward = (this.transform.position - list[i].transform.position).normalized;
        //    }
        //}
        OnDrawLine();
    }
    private void OnDrawLine()
    {
        lineRenderer.positionCount = 0;
        lineRenderer.positionCount = list.Count + 1;
        for (int i = 0; i < list.Count; ++i)
        {
            lineRenderer.SetPosition(i, list[i].position);
        }
        lineRenderer.SetPosition(list.Count, list[0].position);
    }
}
