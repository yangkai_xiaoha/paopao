﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 发力点控制
 */
public class WorkHardControl : MonoBehaviour
{
    //受力半径
    public float radius = 100;
   
    //回弹力
    public float springForce = 1;
    //受阻力
    public float damping = 1.0f;
    //发力点的作用
    public float forceOffset = 3.0f;
    //抖动力的范围
    public Vector2 range = new Vector2(0.1f,1f);
    //发力
    public float force = 100;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private float runTime = 0;
    public float TimeTotle = 1;
    // Update is called once per frame
    void Update()
    {
        runTime += Time.deltaTime;
        forceOffset = Mathf.Lerp(range.x, range.y, runTime / TimeTotle);

        if(runTime > TimeTotle)
        {
            runTime = 0;
        }
    }
    private void OnDestroy()
    {
        ForceManagement.GetItance().RemoveWorkHard(this);
    }
}
