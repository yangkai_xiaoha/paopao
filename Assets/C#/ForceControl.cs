﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 受力的作用
 */
public class ForceControl : MonoBehaviour
{
    public DrawLineControl drawLineControl;

    private List<WorkHardControl> forcePoint;
    //初始位置
    private Vector3 original;
    // Start is called before the first frame update
    void Start()
    {
        original = this.transform.position;
    }
   

    //受力的速度
    private Vector3 velocities;
    private WorkHardControl workHardControl;
    private float forceDistance;
    // Update is called once per frame
    void Update()
    {
        if(!drawLineControl.isForce)
        {
            return;
        }
        forcePoint = ForceManagement.GetItance().getListWorkHard;
        for (int i =0;i< forcePoint.Count; ++i)
        {
            workHardControl = forcePoint[i];
            forceDistance = Vector3.Distance(workHardControl.transform.position, original);
            if (forceDistance < workHardControl.radius)
            {
                Vector3 direction = (original - workHardControl.transform.position).normalized;
                forceDistance = Mathf.Clamp(forceDistance, 0.2f, 2.5f);
                
                velocities += direction * (workHardControl.forceOffset / (forceDistance * drawLineControl.difference)) * Time.deltaTime * 0.1f;
            }
            //恢复到原来的地方
            velocities += (original - this.transform.position) * workHardControl.springForce * Time.deltaTime;//加上+顶点当前位置指向顶点初始位置的速度向量==回弹力
            velocities *= 1f - workHardControl.damping * Time.deltaTime;//乘上阻力
            this.transform.position += velocities * Time.deltaTime;
        }
    }
}
