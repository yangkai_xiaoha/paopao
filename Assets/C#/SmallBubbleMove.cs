﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallBubbleMove : MonoBehaviour
{
    private List<Transform> paths;

    public float moveSpeed = 1;

    private Vector3 targetPos;
    // Start is called before the first frame update
    void Start()
    {
        paths = new List<Transform>();
        Transform pathRoot = GameObject.FindGameObjectWithTag("SmallPaths").transform;
        for (int i = 0; i < pathRoot.childCount; ++i)
        {
            paths.Add(pathRoot.GetChild(i));
        }
        targetPos = paths[Random.Range(0, paths.Count)].position;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position +=(targetPos - this.transform.position).normalized * moveSpeed * Time.deltaTime;

        if(Vector3.Distance(this.transform.position, targetPos)<0.03f)
        {
            targetPos = paths[Random.Range(0, paths.Count)].position;
        }
    }
}
