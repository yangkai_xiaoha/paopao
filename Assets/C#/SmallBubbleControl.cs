﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallBubbleControl : MonoBehaviour
{
    /*
     * 小泡泡控制
     */
    public GameObject[] smallBubble;
    private List<Transform> paths;

    public int length = 40;
    // Start is called before the first frame update
    void Start()
    {
        paths = new List<Transform>();
        Transform pathRoot = GameObject.FindGameObjectWithTag("SmallPaths").transform;
        for (int i = 0; i < pathRoot.childCount; ++i)
        {
            paths.Add(pathRoot.GetChild(i));
        }
        GameObject item;
        //创建小泡泡 
        for (int i =0;i<length;++i)
        {
            item = GameObject.Instantiate(smallBubble[Random.Range(0,3)]);
            item.transform.SetParent(this.transform);
            item.transform.localScale = Vector3.one;
            item.transform.position = paths[Random.Range(0, paths.Count)].position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
