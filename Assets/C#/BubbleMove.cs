﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleMove : MonoBehaviour
{
    private List<Transform> paths;
    private Vector3 startPos;
    private Vector3 endPos;
    private Transform point;
    // Start is called before the first frame update
    void Start()
    {
        paths = new List<Transform>();
        Transform pathRoot = GameObject.FindGameObjectWithTag("Paths").transform;
        for (int i = 0; i < pathRoot.childCount; ++i)
        {
            paths.Add(pathRoot.GetChild(i));
        }
        listIndex = new List<Vector3>();
        FindTarget();
        StartMove();
    }
    List<Vector3> listIndex;
    private float TotalTime = 15.0f;
    public float initTime = 10.0f;
    //设置路径点 2.5 3.0
    private void FindTarget()
    {
        startPos = this.transform.position;
        listIndex.Clear();
        float distacne;
        for (int i = 0; i < paths.Count; ++i)
        {
            distacne = Vector3.Distance(this.transform.position, paths[i].position);
            if (distacne > 2.2f && distacne < 6.5f)
            {
                listIndex.Add(paths[i].position);
            }
        }
        if (listIndex.Count == 0)
        {
            endPos = startPos;
        }
        else
        {
            endPos = listIndex[Random.Range(0, listIndex.Count)];
        }
        distacne = Vector3.Distance(this.transform.position, endPos);
        //TotalTime = initTime * (distacne / 2.0f);
    }
    private bool isMove;
    public void StopMove()
    {
        isMove = false;
    }
    public void StartMove()
    {
        isMove = true;
    }

    public Transform forceTarget;
    Vector3 forcePos;
    Vector3 direction;

    private float moveTime;
    // Update is called once per frame
    void Update()
    {
        forcePos = this.transform.position;
        forcePos.z = 0;
        forceTarget.position = forcePos;
        if (!isMove)
        {
            return;
        }
        //runTime += Time.deltaTime;
        direction = (endPos - startPos).normalized;
        this.transform.position += direction * Time.deltaTime * 0.1f;// Vector3.Lerp(startPos, endPos, runTime / TotalTime);

        if (moveTime > TotalTime)
        {
            FindTarget();
            moveTime = 0;
        }
        moveTime += Time.deltaTime;
    }
}
