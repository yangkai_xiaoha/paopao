﻿using UnityEngine;
using System.Runtime.InteropServices;
using System;
using System.Diagnostics;

public class WindowActive : MonoBehaviour
{

    [DllImport("User32.dll", EntryPoint = "FindWindow")]
    extern static IntPtr FindWindow(string lpClassName, string lpWindowName);

    [DllImport("User32.dll")]
    extern static bool SetForegroundWindow(IntPtr hWnd);

    [DllImport("User32.dll")]
    extern static bool ShowWindow(IntPtr hWnd, short State);

    public float Wait = 20;//延迟执行
    public float Rate = 0.1f;//更新频率
    IntPtr hWnd;
    [DllImport("user32.dll ")]
    public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
    static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
    static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);
    static readonly IntPtr HWND_TOP = new IntPtr(0);
    static readonly IntPtr HWND_BOTTOM = new IntPtr(1);
    const UInt32 SWP_NOSIZE = 0x0001;
    const UInt32 SWP_NOMOVE = 0x0002;
    const UInt32 SWP_NOZORDER = 0x0004;
    const UInt32 SWP_NOREDRAW = 0x0008;
    const UInt32 SWP_NOACTIVATE = 0x0010;
    const UInt32 SWP_FRAMECHANGED = 0x0020;
    const UInt32 SWP_SHOWWINDOW = 0x0040;
    const UInt32 SWP_HIDEWINDOW = 0x0080;
    const UInt32 SWP_NOCOPYBITS = 0x0100;
    const UInt32 SWP_NOOWNERZORDER = 0x0200;
    const UInt32 SWP_NOSENDCHANGING = 0x0400;
    const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;

    void Start()
    {
        hWnd = C.GetProcessWnd();
        //InvokeRepeating("Active", Wait, Rate);
        Invoke("Active", Wait);
    }

    /// <summary>
    /// 激活窗口
    /// </summary>
    void Active()
    {
        try
        {
            ShowWindow(hWnd, 1);
            SetForegroundWindow(hWnd);
            SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
        }
        catch (Exception e)
        {

        }
    }
}

public class C
{

    public delegate bool WNDENUMPROC(IntPtr hwnd, uint lParam);
    [DllImport("user32.dll", SetLastError = true)]
    public static extern bool EnumWindows(WNDENUMPROC lpEnumFunc, uint lParam);

    [DllImport("user32.dll", SetLastError = true)]
    public static extern IntPtr GetParent(IntPtr hWnd);
    [DllImport("user32.dll")]
    public static extern uint GetWindowThreadProcessId(IntPtr hWnd, ref uint lpdwProcessId);

    [DllImport("kernel32.dll")]
    public static extern void SetLastError(uint dwErrCode);


    public static IntPtr GetProcessWnd()
    {
        IntPtr ptrWnd = IntPtr.Zero;
        uint pid = (uint)Process.GetCurrentProcess().Id;  // 当前进程 ID  

        bool bResult = EnumWindows(new WNDENUMPROC(delegate (IntPtr hwnd, uint lParam)
        {
            uint id = 0;
            if (GetParent(hwnd) == IntPtr.Zero)
            {
                GetWindowThreadProcessId(hwnd, ref id);
                if (id == lParam)    // 找到进程对应的主窗口句柄  
                {
                    ptrWnd = hwnd;   // 把句柄缓存起来  
                    SetLastError(0);    // 设置无错误  
                    return false;   // 返回 false 以终止枚举窗口  
                }
            }

            return true;

        }), pid);

        return (!bResult && Marshal.GetLastWin32Error() == 0) ? ptrWnd : IntPtr.Zero;
    }

}