﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleJsonDataReceiveDemo : MonoBehaviour
{
    //偏移量（单位毫米），红外摄像头拍摄比例范围，偏移旋转角度
    //以左上角作为坐标0，右下角为1进行映射，目前映射范围是一个0~4290mm的正方向
    //正方形中心是屏幕中心。
    public Vector2 rect;
    public Vector2 offset243;
    public Vector2 offset244;
    public Vector2 offset245;
    public Vector2 cameraRange;
    public float offsetAngle243;
    public float offsetAngle244;
    public float offsetAngle245;
    //在面板中绑定相应的数据接收script
    public JsonDataReceiveThread jsonDataReceive_243;
    public JsonDataReceiveThread jsonDataReceive_244;
    public JsonDataReceiveThread jsonDataReceive_245;

    //一个信号量，用于标识是否收到了最新数据
    private bool receiveNewJsonData_243 = false;
    private bool receiveNewJsonData_244 = false;
    private bool receiveNewJsonData_245 = false;

    private TUIOJsonStruct receiveTUIOJsonData_243;
    private EllipticJsonStruct receiveEllipticData_243;
    private TUIOJsonStruct receiveTUIOJsonData_244;
    private EllipticJsonStruct receiveEllipticData_244;
    private TUIOJsonStruct receiveTUIOJsonData_245;
    private EllipticJsonStruct receiveEllipticData_245;

    public Camera mainCamera;
    [Tooltip("合并后重新映射之后的坐标，这个将作为示例使用，如需要其他数据请" +
        "根据本demo提供的映射计算方法自行计算")]
    public List<Vector2> remapPositoin;


    //创建接收Json数据的方法
    private void OnJsonRecieved_243(TUIOJsonStruct mTUIOJsonData, EllipticJsonStruct mEllipticJsonData, LadarRawDataStruct mLadarRawData)
    {
        //这里处理收到的数据
        if (!receiveNewJsonData_243)
        {
            receiveTUIOJsonData_243 = mTUIOJsonData;
            receiveEllipticData_243 = mEllipticJsonData;
        }
        receiveNewJsonData_243 = true;
    }
    //创建接收Json数据的方法
    private void OnJsonRecieved_244(TUIOJsonStruct mTUIOJsonData, EllipticJsonStruct mEllipticJsonData, LadarRawDataStruct mLadarRawData)
    {
        //这里处理收到的数据
        if (!receiveNewJsonData_244)
        {
            receiveTUIOJsonData_244 = mTUIOJsonData;
            receiveEllipticData_244 = mEllipticJsonData;
        }
        receiveNewJsonData_244 = true;
    }
    //创建接收Json数据的方法
    private void OnJsonRecieved_245(TUIOJsonStruct mTUIOJsonData, EllipticJsonStruct mEllipticJsonData, LadarRawDataStruct mLadarRawData)
    {
        //这里处理收到的数据
        if (!receiveNewJsonData_245)
        {
            receiveTUIOJsonData_245 = mTUIOJsonData;
            receiveEllipticData_245 = mEllipticJsonData;
        }
        receiveNewJsonData_245 = true;
    }
    public Transform centerPoint;
    public Transform bottomPoint;
    //private float radius;
    void Start()
    {
        //radius = Vector3.Distance(topTarget.position, bottomTarget.position);
        //radius -= 1.2f;


        //绑定事件
        jsonDataReceive_243.OnJsonRecieved += OnJsonRecieved_243;
        jsonDataReceive_244.OnJsonRecieved += OnJsonRecieved_244;
        jsonDataReceive_245.OnJsonRecieved += OnJsonRecieved_245;
    }
    void OnGUI()
    {
        //GUI.Label(new Rect(0, 0, 200, 20), "X:" + "-Y:" );
        //return;
        //ReMapAll(receiveEllipticData_243, receiveEllipticData_244, receiveEllipticData_245);
        //for (int i = 0; i < remapPositoin.Count; i++)
        //{
        //    //左上角 0 0 
        //    float remap_x = (remapPositoin[i].x - 0.5f) * 1.5f + 0.5f;
        //    float remap_y = (remapPositoin[i].y - 0.5f) * 1.5f + 0.5f;

        //    //GUI.Label(new Rect(remapPositoin[i].x * 1600, remapPositoin[i].y * 1600, 200, 20), "X:" + remapPositoin[i].x.ToString() + "-Y:" + remapPositoin[i].y.ToString());
        //    GUI.Label(new Rect(remap_x * 2296, remap_y * 2160, 200, 20), "X:" + remap_x.ToString() + "-Y:" + remap_y.ToString());
        //    Debug.Log("remapPositoin " + i.ToString() + ":" + "X:" + remapPositoin[i].x.ToString() + "-Y:" + remapPositoin[i].y.ToString());
        //}
        //receiveNewJsonData_243 = false;
        //receiveNewJsonData_244 = false;
        //receiveNewJsonData_245 = false;
    }
    void Update()
    {
        
        //Debug.Log("yangkai----:" + Vector3.Distance(topTarget.position, bottomTarget.position));
        ReMapAll(receiveEllipticData_243, receiveEllipticData_244, receiveEllipticData_245);
        //刷新
        RePos();
        receiveNewJsonData_243 = false;
        receiveNewJsonData_244 = false;
        receiveNewJsonData_245 = false;
        /*
        if (receiveNewJsonData_243 && receiveEllipticData_243.Elliptic_x != null)
        {
            //数据刷新放在Update或者OnGUI都可以，这里放在OnGUI了

            Debug.Log(" receiveTUIOJsonData243.TUIO_x[0]:" + receiveTUIOJsonData_243.TUIO_x[0].ToString());
            Debug.Log(" receiveEllipticData243.x[0]:" + receiveEllipticData_243.Elliptic_x[0].ToString());
        }
        if (receiveNewJsonData_244 && receiveEllipticData_243.Elliptic_x != null)
        {
            //数据刷新放在Update或者OnGUI都可以，这里放在OnGUI了

            Debug.Log(" receiveTUIOJsonData244.TUIO_x[0]:" + receiveTUIOJsonData_244.TUIO_x[0].ToString());
            Debug.Log(" receiveEllipticData244.x[0]:" + receiveEllipticData_244.Elliptic_x[0].ToString());
        }
        if (receiveNewJsonData_245 && receiveEllipticData_243.Elliptic_x != null)
        {
            //数据刷新放在Update或者OnGUI都可以，这里放在OnGUI了

            Debug.Log(" receiveTUIOJsonData245.TUIO_x[0]:" + receiveTUIOJsonData_245.TUIO_x[0].ToString());
            Debug.Log(" receiveEllipticData245.x[0]:" + receiveEllipticData_245.Elliptic_x[0].ToString());
        }
        receiveNewJsonData_243 = false;
        receiveNewJsonData_244 = false;
        receiveNewJsonData_245 = false;
        */
    }
    //private RectTransform oring;
    public void RePos()
    {
        int length = remapPositoin.Count;// remapPositoin.Count > player.Count?player.Count: remapPositoin.Count;
        float x;
        float y;
        //if (oring == null)
        //{
        //    oring = GameObject.FindGameObjectWithTag("target").GetComponent<RectTransform>();
        //}
        //
        Vector3 worldVector3;
        Vector3 screenVector3;
        for (int i = 0; i < length; i++)
        {
            x = (((remapPositoin[i].x - 0.5f) * 1.5f) + 0.5f) ;
            y = (((remapPositoin[i].y - 0.5f) * 1.5f) + 0.5f) ;

            y = 1 - y;

            x = 2296 * x;
            y = 2160 * y;

            screenVector3 = Vector3.right * x + Vector3.up * y + Vector3.forward * 2.86f;
            worldVector3 = mainCamera.ScreenToWorldPoint(screenVector3);
            TouchManagement.getItance().TouchWorld(worldVector3);

            //GameObject item = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            //item.transform.localScale = Vector3.one * 0.1f;
            //item.transform.position = worldVector3;
            //item.gameObject.AddComponent<testMain>();

            //判断是加靠近边缘还是单击效果 Vector3.Distance(topTarget.position, bottomTarget.position);
            if (Vector3.Distance(centerPoint.position, worldVector3) > Vector3.Distance(centerPoint.position, bottomPoint.position))
            {
                ForceManagement.GetItance().AddForce(worldVector3);
            }
            

            //screenVector3 = Vector3.right * x + Vector3.up * y + Vector3.forward * 10;
            //player[i].position = worldVector3;// mainCamera.ScreenToWorldPoint(screenVector3);
            //player[i].gameObject.SetActive(true);
            //Debug.Log(player[i].position);
        }

    }

    //通过录入参数进行重新映射，并存储到公共变量中
    private void ReMapAll(EllipticJsonStruct mEllipticJsonData243, EllipticJsonStruct mEllipticJsonData244, EllipticJsonStruct mEllipticJsonData245)
    {
        //清空总列表
        remapPositoin.Clear();
        if (mEllipticJsonData243.Elliptic_x != null)
        {
            //将第一个重新映射
            for (int i = 0; i < mEllipticJsonData243.Elliptic_x.Length; i++)
            {
                //将重映射的点添加到总列表中
                remapPositoin.Add
                    (
                    ReMap(
                        new Vector2(mEllipticJsonData243.Elliptic_x[i], mEllipticJsonData243.Elliptic_y[i]),
                        offsetAngle243,
                        offset243 + rect / 2
                        )
                    );
            }
        }
        if (mEllipticJsonData244.Elliptic_x != null)
        {
            //将第二个重新映射
            for (int i = 0; i < mEllipticJsonData244.Elliptic_x.Length; i++)
            {
                //将重映射的点添加到总列表中
                remapPositoin.Add
                    (
                    ReMap(
                        new Vector2(mEllipticJsonData244.Elliptic_x[i], mEllipticJsonData244.Elliptic_y[i]),
                        offsetAngle244,
                        offset244 + rect / 2
                        )
                    );
            }
        }
        if (mEllipticJsonData245.Elliptic_x != null)
        {
            //将第三个重新映射
            for (int i = 0; i < mEllipticJsonData245.Elliptic_x.Length; i++)
            {
                //将重映射的点添加到总列表中
                remapPositoin.Add
                    (
                    ReMap(
                        new Vector2(mEllipticJsonData245.Elliptic_x[i], mEllipticJsonData245.Elliptic_y[i]),
                        offsetAngle245,
                        offset245 + rect / 2
                        )
                    );
            }
        }
        //所有的点全部隐藏
        //for (int i = 0; i < 50; ++i)
        //{
        //    player[i].gameObject.SetActive(false);
        //}
    }
    //对每个数值进行映射，直接调用全局变量了，懒得封装了，正式使用需要考虑封装
    private Vector2 ReMap(Vector2 src, float offsetAngle, Vector2 offset)
    {
        Vector2 returnVector2 = new Vector2(0, 0);
        //首先要让所有的数据都移动到中心，以摄像头中心作为宣祖安坐标，而不是左上角
        returnVector2 = (src - new Vector2(0.5f, 0.5f));
        //还原为毫米
        returnVector2 *= cameraRange;
        //然后进行旋转
        returnVector2 = new Vector2(returnVector2.x * Mathf.Cos(offsetAngle * Mathf.Deg2Rad) + returnVector2.y * Mathf.Sin(offsetAngle * Mathf.Deg2Rad),
                                    -returnVector2.x * Mathf.Sin(offsetAngle * Mathf.Deg2Rad) + returnVector2.y * Mathf.Cos(offsetAngle * Mathf.Deg2Rad));
        //平移
        returnVector2 += offset;
        //进行缩放映射
        returnVector2 /= rect;
        //还原为左上角为0右下角为1的映射范围
        //returnVector2 += new Vector2(0.5f, 0.5f);
        return returnVector2;
    }
}
