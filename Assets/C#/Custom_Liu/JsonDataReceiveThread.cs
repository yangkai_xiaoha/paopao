﻿/**************************************************************************
Copyright:清尚
Author: Liu
Date:2021-11-26
Description:内存通信接收端，输入通道名称、长度、以byte[]数组作为数据接收后
            存储的介质。另外建议使用委托，避免同时读写全局变量可能产生的画
            面撕裂现象。
Example:
        //创建通道变量
        private static ProcessMailBox MailBoxReceivedDataFromServer = new ProcessMailBox();
        //以通道名+长度在start中初始化
        MailBoxReceivedDataFromServer.InitMailBox("MailBoxEditorToGame", 1024 * 1024 * 4);
        //之后通过GetByteToContent接收数据并转化为string，
        //尽量使用线程进行数据获取，避免阻塞
        receiveJson = System.Text.Encoding.UTF8.GetString(
        MailBoxReceivedDataFromServer.GetByteToContent());
**************************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ThreadMessage.MailBoxes;
using System.Threading;

public class JsonDataReceiveThread : MonoBehaviour
{
    [Tooltip("通道名")]
    public string mailBoxName;
    [Tooltip("通道缓冲区大小")]
    public int mailBoxLength= 1024 * 1024 * 4;
    [Tooltip("接收到的string原始数据")]
    public string receiveJsonData;
    [Tooltip("解析后的TUIO数据")]
    public TUIOJsonStruct receiveTUIOData;
    [Tooltip("解析后的雷达原始数据")]
    public LadarRawDataStruct receiveLadarRawData;
    [Tooltip("解析后的最优椭圆数据")]
    public EllipticJsonStruct receiveEllipticData;
    //内存通道变量
    private ProcessMailBox MailBoxDataReceived = new ProcessMailBox();
    //利用unity3d中的Action直接将数据发送到绑定的方法中
    public System.Action<TUIOJsonStruct, EllipticJsonStruct, LadarRawDataStruct> OnJsonRecieved;
    //用于控制线程运行状态
    private bool exitSignal = true;
    void Start()
    {
        //初始化内存通信通道
        MailBoxDataReceived.InitMailBox(mailBoxName, mailBoxLength);
        //创建接收数据线程，不带参数的可以直接创建，否则需要
        exitSignal = true;
        Thread ReceiveDataThread = new Thread(StartReceiveDataThread);
        ReceiveDataThread.Start();
    }

    //从指定位置接收数据
    public void StartReceiveDataThread()
    {
        //开始循环获取数据
        Debug.Log("StartReceiveDataFromEditor");
        while (exitSignal)
        {
            //receiveJsonData = System.Text.Encoding.UTF8.GetString(MailBoxDataReceived.GetByteToContent());
            receiveJsonData = System.Text.Encoding.Default.GetString(MailBoxDataReceived.GetByteToContent());
            //Debug.Log("receiveJsonData:" + receiveJsonData);
            
            //解析收到的数据
            receiveTUIOData = JsonUtility.FromJson<TUIOJsonStruct>(receiveJsonData);
            receiveLadarRawData = JsonUtility.FromJson<LadarRawDataStruct>(receiveJsonData);
            receiveEllipticData = JsonUtility.FromJson<EllipticJsonStruct>(receiveJsonData);
            //假如没有或者反序列化错误，那么数据是空的
            //Debug.Log("receiveJsonClass.SN:" + receiveTUIOData.Time);
            //将数据发送给其他使用者
            OnJsonRecieved(receiveTUIOData, receiveEllipticData, receiveLadarRawData);
            
        }
    }
    //程序退出时，需要释放手的资源，否则会卡死
    private void OnApplicationQuit()
    {
        Debug.Log("Exit");
        exitSignal = false;
    }
}
