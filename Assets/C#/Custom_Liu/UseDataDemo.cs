﻿/**************************************************************************
Copyright:清尚
Author: Liu
Date:2021-11-26
Description:一个获取数据并使用的Demo，作为参考
**************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseDataDemo : MonoBehaviour
{
    //在面板中绑定相应的数据接收script
    public RealDataReceiveThread realDataReceive;
    //public JsonDataReceiveThread jsonDataReceive;

    //创建一个临时用于绘制的texture2D载体，无特殊用于，可以删除
    public Texture2D texture2D;
    private Vector2Int cameraSize = new Vector2Int(1024, 1024);
    //一个信号量，用于标识是否收到了最新数据
    private bool receiveNewJsonData = false;
    private bool receiveNewRealData = false;
    //使用局部变量而不直接使用全局变量的原因在于假如摄像头数据高于
    //游戏运行帧数，则可能会出现画面撕裂，即UNITY3D绘制跟不上数据传输速度
    //那么使用局部变量进行一次缓冲可以有效解决画面撕裂问题。
    private byte[] receiveRealData;
    private TUIOJsonStruct receiveTUIOJsonData;
    private EllipticJsonStruct receiveEllipticData;
    //public GameObject TargetGameObject;
    //创建接收Json数据的方法
    private void OnJsonRecieved(TUIOJsonStruct mTUIOJsonData, EllipticJsonStruct mEllipticJsonData, LadarRawDataStruct mLadarRawData)
    {
        //这里处理收到的数据
        
        //假如没有绘制呢，那么直接不进行更新，丢弃当前数据
        if (!receiveNewJsonData)
        {
            receiveTUIOJsonData = mTUIOJsonData;
            receiveEllipticData = mEllipticJsonData;
        }
        receiveNewJsonData = true;
    }

    //创建接收RealData数据的方法
    private void OnRealDataRecieved(byte[] receiveData)
    {
        if (!receiveNewRealData)
        {
            receiveRealData = receiveData;
            Debug.Log("receiveRealData:"+ receiveRealData.Length);
        }
        receiveNewRealData = true;
    }
    void Start()
    {
        //绑定事件
        //jsonDataReceive.OnJsonRecieved += OnJsonRecieved;
        realDataReceive.OnRealDataRecieved += OnRealDataRecieved;
        //创建贴图
        texture2D = new Texture2D(cameraSize.x, cameraSize.y, TextureFormat.R8, false, false);
    }
    /*
    void OnGUI()
    {
        
        //数据更新了才会更新texture2D内容
        if (receiveNewRealData&&(receiveRealData.Length!=0))
        {
            texture2D.LoadRawTextureData(receiveRealData);
            texture2D.Apply();
        }
        //绘制是强制的，假如没有更新图案，那么按照之前一帧绘制即可
        GUI.DrawTexture(new Rect(0, 0, cameraSize.x, cameraSize.y), texture2D, ScaleMode.ScaleToFit);

        //需要注意的是传过来的原始图像内容跟unity3d渲染的顺序是不一样的
        //实际传输过来的图像是以左上角为0右下角为最大进行排列的，但unity3d
        //显示的是上下倒转的，所以这里对于点的Y位置做了倒置处理，具体使用
        //需要根据实际情况而定。
        //同样，只有没数据的时候才不绘制，否则绘制上一帧TUIO协议内容
        if (receiveTUIOJsonData.TUIO_x != null)
        {
            GUI.Label(new Rect(0, 0, 200, 200), receiveTUIOJsonData.Time.ToString());
            for (int i = 0; i < receiveTUIOJsonData.TUIO_x.Length; i++)
            {
                GUI.Label(new Rect(receiveTUIOJsonData.TUIO_x[i]*1024.0f, 1024.0f- receiveTUIOJsonData.TUIO_y[i]*1024.0f, 200, 200), receiveTUIOJsonData.ID[i].ToString());
            }
            
        }
        receiveNewRealData = false;
        receiveNewJsonData = false;
        
    }
    */
    void Update()
    {
        //数据刷新放在Update或者OnGUI都可以，这里放在OnGUI了
        //receiveTUIOJsonData.TUIO_x[0]

        receiveNewRealData = false;

    }
}
