﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class VoiceClicp : MonoBehaviour
{
    public RealDataReceiveThread realDataReceive;
    public AudioSource AS;
    private string VoiceUrl;
    // Start is called before the first frame update
    void Start()
    {
        //    StartCoroutine(DoVioceClip());
        realDataReceive.OnRealDataRecieved += OnRealDataRecieved;
    }
    private void OnRealDataRecieved(byte[] receiveData)
    {

        VoiceUrl = Application.persistentDataPath + "/temp.mp3"; //存储路径
        File.WriteAllBytes(VoiceUrl, receiveData);  //写入文件
        
    }
    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator DoVioceClip()
    {
        UnityWebRequest uwr = UnityWebRequestMultimedia.GetAudioClip(VoiceUrl, AudioType.MPEG);
        {
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError)
                Debug.LogError(uwr.error);
            else
            {
                AS.clip = DownloadHandlerAudioClip.GetContent(uwr);
                AS.Play();
            }
        }
    }
}
