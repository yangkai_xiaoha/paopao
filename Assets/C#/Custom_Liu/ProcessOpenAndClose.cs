﻿/**************************************************************************
Copyright:清尚
Author: Liu
Date:2022-1-6
Description:ProcessOpenAndClose提供了读取需要启动应用程序的列表ProcessList.txt，
            并依次启动相应路径下的exe程序。假如exe使用了相对路径的本地文件，那么
            需要创建快捷方式并以启动快捷方式的方式存储路径，否则会出现搜索不到文件
            的情况,ProcessList.txt需要自行创建，目录在调试模式在Assets下，发布后在_data下
Example:

    D:\硬件后台\摄像头后台\Release - 243\GameEditorServer.lnk
    D:\硬件后台\摄像头后台\Release - 244\GameEditorServer.lnk
    D:\硬件后台\摄像头后台\Release - 245\GameEditorServer.lnk
**************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using UnityEngine;

public class ProcessOpenAndClose : MonoBehaviour
{
    //私有启动进程列表
    private List<Process> procList;
    /// <summary>
    /// 读取文件并启动进程
    /// </summary>
    void Start()
    {
        procList = new List<Process>();
        //读取外部文件，保证启动程序时对应exe可以正常启动
        var fileAddress = System.IO.Path.Combine(Application.dataPath, "ProcessList.txt");
        FileInfo fileInfo = new FileInfo(fileAddress);
        string porcessPath = "";
        if (fileInfo.Exists)
        {
            StreamReader r = new StreamReader(fileAddress);
            //StreamReader默认的是UTF8的不需要转格式了，因为有些中文字符的需要有些是要转的，下面是转成String代码
            string readRealData = r.ReadLine();
            byte[] data = new byte[1024];
            data = Encoding.UTF8.GetBytes(readRealData);
            porcessPath = Encoding.UTF8.GetString(data, 0, data.Length);
            while (null != porcessPath)
            {
                UnityEngine.Debug.Log(porcessPath);
                
                // 启动外部程序
                Process proc = Process.Start(porcessPath);
                if (proc != null)
                {
                    // 监视进程退出
                    proc.EnableRaisingEvents = true;
                    // 指定退出事件方法
                    proc.Exited += new EventHandler(proc_Exited);
                }
                procList.Add(proc);
                readRealData = r.ReadLine();
                if (readRealData == null) break;
                data = Encoding.UTF8.GetBytes(readRealData);
                porcessPath = Encoding.UTF8.GetString(data, 0, data.Length);
                System.Threading.Thread.Sleep(500);
            }
        }
    }
    /// <summary>
    /// 退出程序并关闭包含的外部程序，假如使用的时lnk快捷方式可能无法关闭导致
    /// 重复调用的情况，这里正式使用时需要注意。
    /// </summary>
    private void OnApplicationQuit()
    {
        for (int i = 0; i < procList.Count; i++)
        {
            procList[i].CloseMainWindow();
            procList[i].Close();
        }
    }
    /// <summary>
    /// 外部程序退出事件
    /// </summary>
    void proc_Exited(object sender, EventArgs e)
    {
        UnityEngine.Debug.Log(String.Format("外部程序已经退出！"));
    }
}
