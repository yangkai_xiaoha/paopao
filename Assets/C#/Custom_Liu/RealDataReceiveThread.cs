﻿/**************************************************************************
Copyright:清尚
Author: Liu
Date:2021-11-26
Description:内存通信接收端，输入通道名称、长度、以byte[]数组作为数据接收后
            存储的介质。
Example:
        //创建通道变量
        private static ProcessMailBox MailBoxReceivedRealDataFromServer = new ProcessMailBox();
        //以通道名+长度在start中初始化
        MailBoxReceivedRealDataFromServer.InitMailBox("MailBoxEditorToGameDaHengRealData0", 1024 * 1024 * 4);
        //之后通过GetRealByteToContent接收数据，尽量使用线程进行数据获取，避免阻塞
        realTextureData = MailBoxReceivedRealDataFromServer.GetRealByteToContent();
**************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ThreadMessage.MailBoxes;
using System.Threading;
using UnityEngine.Events;

public class RealDataReceiveThread : MonoBehaviour
{
    [Tooltip("通道名")]
    public string mailBoxName;
    [Tooltip("通道缓冲区大小")]
    public int mailBoxLength= 1024 * 1024 * 4;
    [Tooltip("接收到的原始数据")]
    //public byte[] receiveRealData;
    //内存通道变量
    private ProcessMailBox MailBoxReceivedRealData = new ProcessMailBox();
    //用于控制线程运行状态
    private bool exitSignal=true;
    //[Tooltip("接收的原始数据并建立Action将其传输出去"), ReadOnly]
    public UnityAction<byte[]> OnRealDataRecieved;
    void Start()
    {
        //初始化内存通信通道
        MailBoxReceivedRealData.InitMailBox(mailBoxName, mailBoxLength);
        //创建接收数据线程,开启信号
        exitSignal = true;
        Thread ReceiveDataThread = new Thread(StartReceiveDataThread);
        ReceiveDataThread.Start();
    }
    
    void Update()
    {
        
    }

    //从指定位置接收数据
    public void StartReceiveDataThread()
    {
        Debug.Log("StartReceiveRealDataFromEditor");
        while (exitSignal)
        {
            //收到的数据同步更新到全局变量中
            byte[] receiveRealData = MailBoxReceivedRealData.GetRealByteToContent();
            //将收到的数据发送到Action，利用其委托特性，使其可以被其他脚本直接
            //接收数据
            //Debug.Log("receiveRealData"+ mailBoxName+":"+ receiveRealData.Length);
            OnRealDataRecieved(receiveRealData);
        }
    }
    //程序退出时，需要释放手的资源，否则会卡死
    private void OnApplicationQuit()
    {
        Debug.Log("Exit");
        exitSignal = false;
    }
}
