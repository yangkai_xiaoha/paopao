﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintLadarData : MonoBehaviour
{
    public JsonDataReceiveThread jsonDataReceive;
    private bool receiveNewJsonData = false;
    private TUIOJsonStruct receiveTUIOJsonData;

    //创建接收Json数据的方法
    private void OnJsonRecieved(TUIOJsonStruct mTUIOJsonData, EllipticJsonStruct mEllipticJsonData, LadarRawDataStruct mLadarRawData)
    {
        //这里处理收到的数据

        //假如没有绘制呢，那么直接不进行更新，丢弃当前数据
        if (!receiveNewJsonData)
        {
            receiveTUIOJsonData = mTUIOJsonData;
        }
        receiveNewJsonData = true;
    }

    void Start()
    {
        //绑定事件
        jsonDataReceive.OnJsonRecieved += OnJsonRecieved;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnGUI()
    {
        //GL.LoadOrtho();
        //GL.Begin(GL.LINES);
        //GL.Color(Color.red);
        //GL.Vertex3(0.1f, 0.2f, 0);
        //GL.Vertex3(0.4f, 0.6f, 0);
        //GL.End();
        //需要注意的是传过来的原始图像内容跟unity3d渲染的顺序是不一样的
        //实际传输过来的图像是以左上角为0右下角为最大进行排列的，但unity3d
        //显示的是上下倒转的，所以这里对于点的Y位置做了倒置处理，具体使用
        //需要根据实际情况而定。
        //同样，只有没数据的时候才不绘制，否则绘制上一帧TUIO协议内容
        if (receiveTUIOJsonData.TUIO_x != null)
        {
            GUI.Label(new Rect(0, 0, 200, 200), receiveTUIOJsonData.Time.ToString());
            for (int i = 0; i < receiveTUIOJsonData.TUIO_x.Length; i++)
            {
                GUI.Label(new Rect(receiveTUIOJsonData.TUIO_x[i] * 1024.0f, 1024.0f - receiveTUIOJsonData.TUIO_y[i] * 1024.0f, 200, 200), receiveTUIOJsonData.ID[i].ToString());
            }

        }
        receiveNewJsonData = false;

    }
}
