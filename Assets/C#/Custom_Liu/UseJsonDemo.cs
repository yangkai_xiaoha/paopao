﻿/**************************************************************************
Copyright:清尚
Author: Liu
Date:2021-11-26
Description:一个获取数据并使用的Demo，作为参考
**************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseJsonDemo : MonoBehaviour
{
    //在面板中绑定相应的数据接收script
    public JsonDataReceiveThread jsonDataReceive;

    //创建一个临时用于绘制的texture2D载体，无特殊用于，可以删除
    private Vector2Int cameraSize = new Vector2Int(1024, 1024);
    //一个信号量，用于标识是否收到了最新数据
    private bool receiveNewJsonData = false;
    //使用局部变量而不直接使用全局变量的原因在于假如摄像头数据高于
    //游戏运行帧数，则可能会出现画面撕裂，即UNITY3D绘制跟不上数据传输速度
    //那么使用局部变量进行一次缓冲可以有效解决画面撕裂问题。
    private LadarRawDataStruct receiveLadarRawData;
    private TUIOJsonStruct receiveTUIOJsonData;
    private EllipticJsonStruct receiveEllipticData;
    public bool isCamera;
    //创建接收Json数据的方法
    private void OnJsonRecieved(TUIOJsonStruct mTUIOJsonData, EllipticJsonStruct mEllipticJsonData, LadarRawDataStruct mLadarRawData)
    {
        //这里处理收到的数据

        //假如没有绘制呢，那么直接不进行更新，丢弃当前数据
        if (!receiveNewJsonData)
        {
            receiveLadarRawData = mLadarRawData;
            receiveTUIOJsonData = mTUIOJsonData;
            receiveEllipticData = mEllipticJsonData;
        }
        receiveNewJsonData = true;
    }

    void Start()
    {
        //绑定事件
        jsonDataReceive.OnJsonRecieved += OnJsonRecieved;
    }

    void Update()
    {
        if (receiveNewJsonData)
        {
            //数据刷新放在Update或者OnGUI都可以，这里放在OnGUI了
            if (isCamera)
            {
                Debug.Log(" receiveTUIOJsonData.TUIO_x[0]:" + receiveTUIOJsonData.TUIO_x[0].ToString());
                Debug.Log(" receiveEllipticData.x[0]:" + receiveEllipticData.Elliptic_x[0].ToString());
            }
            else
            {
                Debug.Log(" receiveTUIOJsonData.TUIO_x[0]:" + receiveTUIOJsonData.TUIO_x[0].ToString());
                Debug.Log(" receiveLadarRawData.x[0]:" + receiveLadarRawData.x[0].ToString());
            }
        }
        receiveNewJsonData = false;
    }
}
