﻿/**************************************************************************
Copyright:清尚
Author: Liu
Date:2021-11-26
Description:Json序列化和反序列化的类依据，同时为面板提供ReadOnly属性，保证
            想要保护的数据不被修改。别删除，放在项目中即可
**************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
/*
//以下内容为面板ReadOnly使用，避免从面板改动，忽略这段代码即可
#region
/// <summary>
/// 只读特性功能类
/// </summary>
public class ReadOnlyAttribute : PropertyAttribute { }
/// <summary>
/// 面板绘制
/// </summary>
[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyDrawer : PropertyDrawer
{
    /// <summary>
    /// 用来保持原有高度
    /// </summary>
    /// <param name="property"></param>
    /// <param name="label"></param>
    /// <returns></returns>
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    /// <summary>
    /// 只读
    /// </summary>
    /// <param name="position"></param>
    /// <param name="property"></param>
    /// <param name="label"></param>
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        GUI.enabled = false;
        EditorGUI.PropertyField(position, property, label, true);
        GUI.enabled = true;
    }
}
#endregion
*/
//以下内容为Jsong序列化和反序列化基础类，委托时需要引用
#region
//最优椭圆数据结构，相对屏幕左上角为（0,0），右下角为(1,1)
public struct EllipticJsonStruct
{
    //设备SN编号
    public string SN;
    //时间戳，不同的时间戳表示不同内容
    public long Time;
    //最优椭圆X坐标,范围0.0~1.0
    public float[] Elliptic_x;
    //最优椭圆Y坐标,范围0.0~1.0
    public float[] Elliptic_y;
    //最优椭圆height,范围0.0~1.0
    public float[] Elliptic_height;
    //最优椭圆width,范围0.0~1.0
    public float[] Elliptic_width;
    //最优椭圆angle
    public float[] Elliptic_angle;
}
//TUIO数据结构，相对屏幕左上角为（0,0），右下角为(1,1)
public struct TUIOJsonStruct
{
    //设备SN编号
    public string SN;
    //时间戳，不同的时间戳表示不同内容
    public long Time;
    //TUIO部分ID编号
    public int[] ID;
    //TUIO部分X坐标,范围0.0~1.0
    public float[] TUIO_x;
    //TUIO部分Y坐标,范围0.0~1.0
    public float[] TUIO_y;
    //TUIO部分X轴加速度，基于x坐标
    public float[] TUIO_X;
    //TUIO部分Y轴加速度，基于y坐标
    public float[] TUIO_Y;
    //TUIO部分总体轴加速度
    public float[] TUIO_m;
}

//TUIO数据结构，相对屏幕左上角为（0,0），右下角为(1,1)
public struct LadarRawDataStruct
{
    //设备SN编号
    public string SN;
    //时间戳，不同的时间戳表示不同内容
    public long Time;
    //雷达原始X坐标,范围0.0~1.0
    public float[] x;
    //雷达原始Y坐标,范围0.0~1.0
    public float[] y;
}

#endregion
