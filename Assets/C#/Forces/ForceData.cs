﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceData : MonoBehaviour
{
    public bool isOver;

    public Vector3 Pos;

    private float runTime;

    
    public void Refresh()
    {
        runTime = 0;
        isOver = false;
    }

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        runTime += Time.deltaTime;

        if(runTime > 0.8f)
        {
            ForceManagement.GetItance().RemoveForce(this);
            isOver = true;
            //销毁对象
            Destroy(this.gameObject);
        }
    }
}
