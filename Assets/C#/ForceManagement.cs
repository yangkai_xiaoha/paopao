﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceManagement : MonoBehaviour
{
    /*
     * 受力点管理 
     */
    private List<ForceData> list_Force;

    private static ForceManagement foreceManagement;
    //原点
    //private Transform origin;

    Vector3 origin = new Vector3(0,1,0.3f);
    public ForceManagement()
    {
        listWorkHardControl = new List<WorkHardControl>(); 
    }

    public static ForceManagement GetItance()
    {
        if(foreceManagement ==null)
        {
            GameObject item = new GameObject("ForceManagement");
            
            foreceManagement = item.AddComponent<ForceManagement>();
        }
        return foreceManagement;
    }
    //移除受力数据
    public void RemoveForce(ForceData forceData)
    {
        if(list_Force.Contains(forceData))
        {
            list_Force.Remove(forceData);
            RemoveWorkHard(forceData.gameObject.GetComponent<WorkHardControl>());
        }
    }

    GameObject force;
    //判断是否可以添加
    public void AddForce(Vector3 pos)
    {
        
        if(list_Force ==null)
        {
            list_Force = new List<ForceData>();
        }
        ForceData force;
        float distance;
        bool isExceed = true;

        foreach(var item in list_Force)
        {
            distance = Vector3.Distance(pos, item.Pos);
            Debug.Log(distance);
            if (distance < 0.5f)
            {
                isExceed = false;
                item.Refresh();
                break;
            }
        }

        //靠近边缘的相关逻辑
        if (isExceed)
        {
            GameObject item = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            item.transform.localScale = Vector3.one * 0.5f;
            Vector3 direction = (pos - origin).normalized;
            item.transform.position = pos + direction * 0.3f;
            force = item.AddComponent<ForceData>();
            WorkHardControl work = item.AddComponent<WorkHardControl>();
            work.range = new Vector2(25, 30);
            AddWorkHardControl(work);
            force.Pos = pos;
            list_Force.Add(force);
        }
    }

    private  List<WorkHardControl> listWorkHardControl;

    public  void AddWorkHardControl(WorkHardControl workHard)
    {
        listWorkHardControl.Add(workHard);
    }
    public  void RemoveWorkHard(WorkHardControl workHard)
    {
        if(listWorkHardControl.Contains(workHard))
        {
            listWorkHardControl.Remove(workHard);
        }
    }

    public  List<WorkHardControl> getListWorkHard
    {
        get { return listWorkHardControl; }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
