﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleControl : MonoBehaviour
{
    public ParticleSystem[] listParticle;
    // Start is called before the first frame update
    void Start()
    {
        listParticle[index].Play();
        index++;
    }
    private float runTime;
    private int index;
    // Update is called once per frame
    void Update()
    {
        runTime += Time.deltaTime;

        if(runTime > 2.5)
        {
            runTime = 0;
            if(index == listParticle.Length)
            {
                GameObject.Destroy(this);
                return;
            }
            listParticle[index].Play();
            index++;
        }
    }
}
