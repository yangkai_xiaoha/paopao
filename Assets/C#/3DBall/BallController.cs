﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

public class BallController : MonoBehaviour
{
    public GameObject target;
    public List<BallItem> ballList = new List<BallItem>();
    //所有小球速度限制.
    public float speed = 2;
    //旋转速度.
    public float angleSpeed = 2;
    public GameObject ballParnet;

    //private Action<BallItem> OnClickBall;
    //private Action<BallItem> OnHideBall;
    private static BallController instance;
    public static BallController Instance
    {
        get
        {
            return instance;
        }
    }
    private void Start()
    {
        instance = this;
        #region 大球
        List<BallItem> b1=InsBall(4, "BallItem");
        List<BallItem> b2 = InsBall(4, "BallItem2");
        List<BallItem> b3 = InsBall(4, "BallItem3");

        ballList.AddRange(b1);
        ballList.AddRange(b2);
        ballList.AddRange(b3);
        for (int i=0;i<ballList.Count;i++)
        {
            ballList[i].StartMove(Random.Range(0,0.5f),Random.Range(0, 0.5f));
            ballList[i].AddForce(target.transform.position-ballList[i].transform.position);
        }
        #endregion
        #region 小球.
        List<BallItem> l1 = InsBall(6, "LittleBallItem");
        List<BallItem> ls2 = InsBall(6, "LittleBallItem2");
        List<BallItem> ls3 = InsBall(6, "LittleBallItem3");
        for (int i = 0; i < l1.Count; i++)
        {
            l1[i].isLittle = true;
            l1[i].StartMove(Random.Range(4, 6), Random.Range(2, 4));
            l1[i].AddForce(target.transform.position - l1[i].transform.position);

            l1[i].transform.localScale = Random.Range(1f, 2.5f) * l1[i].transform.localScale;
        }
        ballList.AddRange(l1);

        for (int i = 0; i < ls2.Count; i++)
        {
            ls2[i].isLittle = true;
            ls2[i].StartMove(Random.Range(4, 6), Random.Range(2, 4));
            ls2[i].AddForce(target.transform.position - ls2[i].transform.position);
            ls2[i].transform.localScale = Random.Range(1f, 2.5f) * ls2[i].transform.localScale;
        }
        ballList.AddRange(ls2);
        for (int i = 0; i < ls3.Count; i++)
        {
            ls3[i].isLittle = true;
            ls3[i].StartMove(Random.Range(4, 6), Random.Range(2, 4));
            ls3[i].AddForce(target.transform.position - ls3[i].transform.position);
            ls3[i].transform.localScale = Random.Range(1f, 2.5f) * ls3[i].transform.localScale;
        }
        ballList.AddRange(l1);
        ballList.AddRange(ls2);
        ballList.AddRange(ls3);
        #endregion
        int layer1 = LayerMask.NameToLayer("LittleBall");
        int layer2 = LayerMask.NameToLayer("Wall");
        Physics.IgnoreLayerCollision(layer1, layer2);
    }

    public void ResetAddForce(BallItem item)
    {
        item.AddForce(target.transform.position - item.transform.position);
    }

    List<BallItem> InsBall(int count,string path)
    {
        GameObject go = Resources.Load("Prefabs/" + path) as GameObject;
        BallItem item;
        List<BallItem> balls = new List<BallItem>();
        for (int i=0;i<count;i++)
        {
           GameObject ball= Instantiate(go, ballParnet.transform);
            ball.transform.localPosition = new Vector3(Random.Range(-20f,20f),0,Random.Range(-11f,0));
            item = ball.GetComponent<BallItem>();
            balls.Add(item);
        }
        return balls;
    }
    ///// <summary>
    ///// 注册点击回调.
    ///// </summary>
    ///// <param name="action"></param>
    //public void RegistBallShow(Action<BallItem> action)
    //{
    //    if (action != null)
    //    {
    //        OnClickBall = action;
    //    }

    //}
    ///// <summary>
    ///// 注册关闭回调.
    ///// </summary>
    ///// <param name="action"></param>
    //public void RegistBallHide(Action<BallItem> action)
    //{
    //    if (action != null)
    //    {
    //        OnHideBall = action;
    //    }
    //}

    //public void ClickBall(BallItem ball)
    //{
    //    if (OnClickBall!=null)
    //    {
    //        OnClickBall(ball);
    //    }

    //}

    //public void HideBall(BallItem ball)
    //{
    //    if()
    //}
}
