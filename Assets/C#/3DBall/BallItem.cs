﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Video;

public class BallItem : MonoBehaviour
{
    public float strength = 100;
    public float waitTime = 0;
    public GameObject objPlane;
    public VideoPlayer vp;
    public bool isLittle = false;
    Rigidbody rig;
    [SerializeField]
    float limitSpeed = 0;
    [SerializeField]
    float angleSpeed = 0;
    bool canMove = false;
    Vector3 lastVelocity;
    float orgMass = 0;
    Vector3 orgScale;
    Vector3 orgAngularVelocity;
    public Renderer ren;
    private void Awake()
    {
        rig = GetComponent<Rigidbody>();
        orgMass = rig.mass;
        orgScale = transform.localScale;
        //rig.AddForce((wp - transform.position) * strength);

        if (objPlane != null)
        {
            //matPlane = objPlane.GetComponent<Renderer>().material;
           
        }
    }
    private void FixedUpdate()
    {
        if (!canMove) return;
        //Debug.Log(rig.velocity.normalized);
        //rig.velocity = new Vector3(Mathf.Clamp(rig.velocity.x,BallController.Instance. speedMin.x, BallController.Instance.speedMax.x), 0, Mathf.Clamp(rig.velocity.z, BallController.Instance.speedMin.y, BallController.Instance. speedMax.y));

        //Debug.Log(rig.angularVelocity);
        //Debug.Log("velocity=>"+rig.velocity);
        //Debug.Log("angularVelocity=>" + rig.angularVelocity);
        //RandomDir(rig.velocity.x);
        //RandomDir(rig.velocity.z);
        rig.velocity = new Vector3(ChangeSpeed(rig.velocity.x), 0, ChangeSpeed(rig.velocity.z));
        //rig.angularVelocity = new Vector3(rig.velocity.z,0, rig.velocity.x)*-1;
        //rig.angularVelocity = new Vector3(ChangeSpeed(rig.angularVelocity.x), ChangeSpeed(rig.angularVelocity.y), ChangeSpeed(rig.angularVelocity.z));
    }
    /// <summary>
    /// 开始移动.
    /// </summary>
    public void StartMove(float moeve,float rotate)
    {
        canMove = true;
        limitSpeed = BallController.Instance.speed + moeve;
        angleSpeed = BallController.Instance.angleSpeed + rotate;
    }
    public void AddForce(Vector3 target)
    {
        rig.AddForce((target - transform.position) * strength);
    }
    /// <summary>
    /// 速度限制.
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>

    float ChangeSpeed( float s)
    {
        if (s <0)
        {
            s = limitSpeed * -1;
        }
        else if(s>0)
        {
            s = limitSpeed;
        }
        //else
        //{
        //   s= RandomDir(s);
        //}

        return s;
    }

    /// <summary>
    /// 随机方向.
    /// </summary>
    /// <param name="s"></param>
    void RandomDir(float s)
    {
        if (s == 0)
        {
            waitTime += Time.deltaTime;
            if (waitTime >= 3)
            {
                waitTime = 0;
                BallController.Instance.ResetAddForce(this);
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        //每次碰撞后随机一个速度上限，保证所有小球有速度差.
        limitSpeed = BallController.Instance.speed + Random.Range(0f, 1f);
    }
    bool isStop =false;
    bool isAni = false;
    private void OnMouseDown()
    {
        return;
        if (isLittle) return;
        if (isAni) return;
        isStop = !isStop;
        if (!isStop)
        {
            HideVideo();
            return;
        }
        canMove = false;
        lastVelocity = rig.velocity;
        rig.velocity = Vector3.zero;
        orgAngularVelocity = rig.angularVelocity;
        rig.isKinematic = true;
        rig.mass = 100;

        //transform.localScale = orgScale * 1.5f;
        isAni = true;
        transform.DOScale(orgScale * 1.5f, 2f).SetEase(Ease.OutBack).OnComplete(() => {

            ShowVideo();
        });
    }
    private float currentTime;
    private float lastTime;
    public void OnClickYK()
    {
        currentTime = Time.time;
        if((currentTime - lastTime) < 1.0f)
        {
            //Debug.Log((currentTime - lastTime));
            return;
        }
        if (isLittle) return;
        if (isAni) return;
        isStop = !isStop;
        if (!isStop)
        {
            lastTime = Time.time;
            HideVideo();
            return;
        }
        Debug.Log("OnClickYK:"+ (currentTime - lastTime));
        canMove = false;
        lastVelocity = rig.velocity;
        rig.velocity = Vector3.zero;
        orgAngularVelocity = rig.angularVelocity;
        rig.isKinematic = true;
        rig.mass = 100;
        lastTime = Time.time;
        //transform.localScale = orgScale * 1.5f;
        isAni = true;
        transform.DOScale(orgScale * 1.5f, 2f).SetEase(Ease.OutBack).OnComplete(() => {

            ShowVideo();
        });
    }


    /// <summary>
    /// 恢复运动.
    /// </summary>
    public void ResumeMove()
    {
        canMove = true;
        rig.isKinematic = false;
        rig.velocity= lastVelocity;
        rig.angularVelocity = orgAngularVelocity;
        rig.mass = orgMass;
        transform.localScale = orgScale;


    }

    /// <summary>
    /// 动画播放完毕，开始播放视频.
    /// </summary>
    void ShowVideo()
    {
        isAni = false;


        objPlane.SetActive(true);
        Vector3 tar = BallController.Instance.target.transform.position;
        Vector3 tar2 = new Vector3(tar.x, objPlane.transform.position.y, tar.z);
        objPlane.transform.forward = (tar2 - objPlane.transform.position) * -1;
        //objPlane.transform.localEulerAngles = new Vector3(0, objPlane.transform.localEulerAngles.y, 0);
        //objPlane.transform.localPosition = new Vector3(0, objPlane.transform.localPosition.y, 0);
        RaycastHit hit;
        Physics.Raycast(Camera.main.transform.position, transform.position - Camera.main.transform.position, out hit);
        //if (hit.point)
        //{
        if (hit.collider != null)
        {
            objPlane.transform.position = hit.point;

            objPlane.transform.position = new Vector3(hit.point.x, hit.point.y + 1, hit.point.z);
            objPlane.transform.position = objPlane.transform.position + (tar2 - objPlane.transform.position).normalized * 0.5f;
        }
        vp.time = 0;
        vp.Play();
        ren.material.mainTexture = vp.texture;
    }
    /// <summary>
    /// 关闭视频，小球恢复运动.
    /// </summary>
    void HideVideo()
    {
        objPlane.SetActive(false);
        vp.Stop();
        //小球恢复大小
        transform.DOScale(orgScale, 2f).SetEase(Ease.OutBack).OnComplete(() => {
            isAni = false;
            ResumeMove();
        });
 
    }
}
