﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testMain : MonoBehaviour
{
    public Transform point;
    private Vector2 screenVector2;

    public Camera mCamera;

    public BallController ballControl;
    // Start is called before the first frame update
    void Start()
    {
        screenVector2 = Screen.width * Vector2.right + Vector2.up * Screen.height;
    }
    // Update is called once per frame 0.4635048:0.9692118 2.86
    void FixedUpdate()
    {
        //Debug.Log("Point："+ mCamera.WorldToScreenPoint(point.position).z);
        if(Input.GetKeyDown(KeyCode.S))
        {
            Vector2 mousePosition = Input.mousePosition;
            Vector2 rang = Vector2.right * (mousePosition.x / screenVector2.x) + Vector2.up * (mousePosition.y / screenVector2.y);
            Debug.Log(rang.x+":"+ rang.y);
        }

        if(Input.GetKeyDown(KeyCode.G))
        {
            //TouchManagement.getItance().TouchWorld(worldVector3);
            


            GameObject item = GameObject.CreatePrimitive(PrimitiveType.Cube);
            item.transform.localScale = Vector3.one * 0.3f;
            item.transform.position = mCamera.ScreenToWorldPoint(Vector3.right * 0.4635048f*Screen.width +
                Vector3.up * Screen.height * 0.9692118f+Vector3.forward * 2.86f);
        }

       
        if (Input.GetMouseButton(1))
        {
            RaycastHit hit;
            Ray ray = mCamera.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.blue);
            bool isRayCast = Physics.Raycast(ray.origin, ray.direction, out hit);

            if (isRayCast && hit.collider.gameObject.tag == "paopao")
            {
                BallItem ballItem = hit.collider.gameObject.GetComponent<BallItem>();
                if (ballItem != null)
                    ballItem.OnClickYK();

                //Debug.Log(hit.collider.gameObject.name);
            }
        }
    }  
}
