﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
public class BubbleControl : MonoBehaviour
{
    private BubbleMove bubbleMove;
    private Image uiPaoPao;
    public VideoPlayer videoPlayer;
    public RawImage videoRawImage;
    // Start is called before the first frame update
    void Start()
    {
        bubbleMove = this.gameObject.GetComponent<BubbleMove>();
        uiPaoPao = this.gameObject.GetComponent<Image>();
    }
    private bool isScal;
    //点击触发 bubbleMove.StartMove();
    public void OnClick()
    {
        if (isScal)
        {
            return;
        }
        isScal = true;
        bubbleMove.StopMove();
        start = uiPaoPao.rectTransform.sizeDelta;
        if (start.x > 280)
        {
            videoPlayer.Stop();
            videoRawImage.enabled = false;
            end = Vector2.one * 150;
            isPlay = false;
        }
        else
        {

            end = Vector2.one * 300;
        }
    }
    private Vector2 start;
    private Vector2 end;
    private float runTime;

    public Transform orign;

    Vector3 localEulerAngles;

    //检测什么时候播放停止
    private bool isPlay;
    // Update is called once per frame
    void Update()
    {
        if (isPlay)
        {
            if (videoPlayer.frameRate + 10 > videoPlayer.frameCount)
            {
                OnClick();
            }
        }

        this.transform.up = (orign.position - this.transform.position).normalized;
        localEulerAngles = this.transform.localEulerAngles;
        localEulerAngles.x = 0;
        localEulerAngles.y = 0;
        this.transform.eulerAngles = localEulerAngles;
        if (!isScal)
        {
            return;
        }
        runTime += Time.deltaTime;
        if (runTime > 3)
        {
            runTime = 0;
            if (uiPaoPao.rectTransform.sizeDelta.x > 280)
            {
                videoRawImage.enabled = true;
                videoPlayer.Play();
                isPlay = true;
            }
            else
            {
                bubbleMove.StartMove();
            }
            isScal = false;
            return;
        }
        uiPaoPao.rectTransform.sizeDelta = Vector2.Lerp(start, end, runTime / 3);
    }
}
