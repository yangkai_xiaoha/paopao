﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineControl : MonoBehaviour
{
    public DrawLineControl drawLineControl;
    public Material[] mats;
    private void Awake()
    {
        
    }
    public List<Vector3> listPos;
    // Start is called before the first frame update  
    void Start()
    {
        ///listWorkHardControl = new List<WorkHardControl>();
        DrawLineControl item;
        float radius = drawLineControl.radius;
        float total = 0;
        for (int i = 1; i < 17; i++)
        {
            item = GameObject.Instantiate<DrawLineControl>(drawLineControl);
            item.GetComponent<LineRenderer>().material = mats[i];
            item.transform.SetParent(this.transform);
            item.transform.position = Vector3.zero;
            item.transform.position = listPos[i];// drawLineControl.transform.position;
            //item.transform.position = drawLineControl.transform.forward * i * 0.09f;
            item.radius = radius - i * 0.04f;
            total = i * 0.15f;
            //item.difference = i * 0.15f * 0.1f;
            
            if (i > 15)
            {
                drawLineControl.isForce = false;
            }
        }
    }
  
    // Update is called once per frame
    void Update()
    {
        
    }
} //64.05
  // 1.15
  //1.16
  //1.17
  //1.19
  //1.22
  //1.25
  //1.27
  //1.29
  //1.3
  //1.32
  //1.35
  //1.4
  //1.45
  //1.52
  //1.56
  //1.62
  //1.63