﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainTest : MonoBehaviour
{
    private Camera mCamera;
    public Transform target;
    // Start is called before the first frame update
    void Awake()
    {
        mCamera = this.gameObject.GetComponent<Camera>();

        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 screenVectro3 = mCamera.WorldToScreenPoint(target.position);

        Vector2 screenVector2 = new Vector2(screenVectro3.x / 1600, screenVectro3.y / 1600);
        Debug.Log(screenVector2 + " : " + mCamera.WorldToScreenPoint(target.position));
    }
}
