﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointAnimationControl : MonoBehaviour
{
    public WorkHardControl workhardControl;

    public float timeTotal = 5.0f;

    public Vector3 start = new Vector3(0,90,0);
    public Vector3 end = new Vector3(0, -90, 0);

    public bool isStart;
    // Start is called before the first frame update
    void Start()
    {
        ForceManagement.GetItance().AddWorkHardControl(workhardControl);
        if (isStart)
        {
            workhardControl.range = Vector2.one * 0.5f;
        }
        else
        {
            workhardControl.range = Vector2.one * 0.1f;
        }
        this.transform.eulerAngles = start;
    }
    
    private float runTime;
    // Update is called once per frame
    void Update()
    {
        runTime += Time.deltaTime;
        this.transform.eulerAngles = Vector3.Lerp(start,end, runTime / timeTotal);

        if(runTime > timeTotal)
        {
            isStart = !isStart;
            if(isStart)
            {
                workhardControl.range = Vector2.one * 0.5f;
            }
            else
            {
                workhardControl.range = Vector2.one * 0.1f;
            }
            runTime = 0;
        }
    }
}
